# object_detection

This folder give several ways to detect object with their colour. 
I used these algorithms for a Pick&Place for PR2.

# Launch Only the algorithm #
**Launch the Kinect :** 
```
#!ROS
roslaunch openni_launch openni.launch publish_tf:=false camera:="/camera/depth/disparity"
```
**Launch the color Algorithm :**

```
#!ROS 
rosnode object_detection ColorTrackingEssaiKinect
```

** See topics send : **

```
#!ROS 
rostopic echo /pyride_status
```

# If you want to use this algorithm with Pyride (RealRobot) and the Duplo Interface #

```
#!ROS 
export ROS_MASTER_URI=http://pr2:11311
export ROS_MASTER_URI=http://rosim:11311 (connect to another machine)
roslaunch openni_launch openni.launch publish_tf:=false camera:="head_mount_kinect" depth_frame_id:="head_mount_kinect_ir_optical_frame"
roslaunch pyride_pr2 pyride.launch
```
*Don't forget to put your script under the pyride file*

* Launch the API and use the website *

## Debugger... ##
```
#!ROS

~/.ros$ tail -f pyride_pr2.log 
```


# If you want to use this algorithm with Pyride (GazeboRobot) and the Duplo Interface #

```
#!ROS 
roslaunch gazebo_ros empty_world.launch
roslaunch pr2_gazebo pr2.launch
roslaunch pyride_pr2 pyride.launch
```
## Then, insert the object in the scene ##



```
#!ROS

rosrun gazebo_ros spawn_model -file `pwd`/catkin_ws/src/SpawnObjectGazebo/Table.urdf -urdf -z 1 -model my_table
rosrun gazebo_ros spawn_model -file `pwd`/catkin_ws/src/SpawnObjectGazebo/Duplo.urdf -urdf -z 1 -model my_object
rosrun gazebo_ros spawn_model -file `pwd`/catkin_ws/src/SpawnObjectGazebo/Duplo2.urdf -urdf -z 1 -model my_object2
```



## Moving the robot under Pyride ##




```
#!ROS

PyPR2.moveHeadTo(-0.2,0.7)
pose={'position':(0.70,0.35,0.66),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':True}
PyPR2.moveArmTo(**pose)

joint={'r_wrist_flex_joint': 1.7,'r_wrist_roll_joint': 1.7, 'r_forearm_roll_joint': 0, 'r_elbow_flex_joint': -1, 'r_shoulder_lift_joint': 0.8, 'r_upper_arm_roll_joint': -1,  'r_shoulder_pan_joint': -0.85 }    
PyPR2.moveArmWithJointPos(**joint)
```


* Launch the API and use the website *





# Description #

I have used OPENCV and PointCloud.

# Algorithme under OpenCV : # 
 - Uses a Canny Filter to detect the edges in my images
 - Once I have done that, I use HSV data to determine the range of the Hue, Saturation, and Value of my color
 - Then It detects the color. 

OPENCV :

![alt text](Image/OPENCV.png " Image with OpenCV Detection")

PCL :

![alt text](Image/PCL1.png " Image with PCL Detection")
![alt text](Image/PCL2.png " Image with PCL Detection")
![alt text](Image/PCL3.png " Image with PCL Detection")
![alt text](Image/PCL4.png " Image with PCL Detection")